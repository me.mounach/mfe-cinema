import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'auth-lib';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { App } from '../../models/app/app';
import { Cinema } from '../../models/cinema/cinema';
import { City } from '../../models/city/city';
import { CinemaService } from '../../services/cinema.service';
import { CityService } from '../../services/city.service';
import {Location, Appearance} from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;

@Component({
  selector: 'app-cinemas',
  templateUrl: './cinemas.component.html',
  styleUrls: ['./cinemas.component.scss']
})
export class CinemasComponent implements OnInit {

  /* ----------------------- */
  cinemas: Array<Cinema> = new Array();
  cities: Array<City> = new Array();
  selectedCity: string = "0";
  cityExist: boolean = true;
  cinema: Cinema = new Cinema();
  newCity: City = new City();
  app: App = new App();

  public appearance = Appearance;
  public selectedAddress!: PlaceResult;


  /* ----------------------- */
  constructor(private cinemaService: CinemaService,
              private cityService: CityService,
              config: NgbModalConfig, 
              private authService: AuthService,
              private modalService: NgbModal,
              private router: Router,
              @Inject(SESSION_STORAGE) private storage: StorageService) { 
        config.backdrop = 'static';
        config.keyboard = false;
    }

  /* ----------------------- */
  ngOnInit(): void {

    this.getUser().then((data: any) => {
      
      if(data.body.isLogged == true) {
        this.getAllCinemas();
        this.getAllCities();
      }else{
        this.app.email = undefined; 
        this.app.status = 'NOT AUTHORIZED';
        this.app.username = '*** NONE ***'; 
        this.app.role = undefined;
        
        this.storage.set("app", this.app);
        this.router.navigate(['/home']);
      }

    }).catch(err => {
        this.app.error = true; 
        this.app.email = undefined; 
        this.app.status = 'NOT AUTHORIZED';
        this.app.username = '*** NONE ***'; 
        this.app.role = undefined;
        
        this.storage.set("app", this.app);
        this.router.navigate(['/home']);
    });


  }

  //------------
  getUser() {
    let promise = new Promise((resolve, reject) => {

        this.authService.getUser()
        .subscribe((response: any) => {

          resolve(response);

        } ,
          (err: any) => {
          reject(err);
        }
      );
    });
    
    return promise;
  }

  /* ----------------------- */
  getAllCinemas() {

    let promise = new Promise((resolve, reject) => {
      this.cinemaService.getAllCinemas()
     .subscribe((response: any) => {
       //console.log(response);
       this.cinemas = response.body.cinemas;
       resolve(response);
       } ,
     err => {
       //console.log(  err.status );
       reject(err);
      });
    });

    return promise;
  }

  /* ----------------------- */
  getAllCities() {

    let promise = new Promise((resolve, reject) => {
      this.cityService.getAllCities()
     .subscribe((response: any) => {
       //console.log(response);
       this.cities = response.body.cities;
       resolve(response);
       } ,
       (err: any) => {
       //console.log(  err.status );
       reject(err);
      });
    });

    return promise;
  }

  /* ----------------------- */
  deleteCinema(id: any) {
    this.deleteCinemaService(id).then((response: any) => {
      this.getAllCinemas();
      this.modalService.dismissAll();
    });
  }

  /* open delete modal */
  openDeleteModal(content: any) {
    this.modalService.open(content, {centered: true});
  }

  openCreateModal(content: any) {
    this.modalService.open(content, { size: 'lg', scrollable: true });
    this.getAllCities();
    this.cityExist = true;
    this.selectedCity = "0";
    this.cinema = new Cinema();
  }

  openUpdateModal(content: any, c: Cinema) {
    this.modalService.open(content, { size: 'lg', scrollable: true });
    this.getAllCities();
    this.cityExist = true;
    this.cinema = c;
    this.selectedCity = c.city?._id as string;
  }

  /* ----------------------- */
  deleteCinemaService(id: string) {
    let promise = new Promise((resolve, reject) => {
      this.cinemaService.deleteCinema(id)
     .subscribe((response: any) => {
       //console.log(response);
       resolve(response);
       } ,
     err => {
       //console.log(  err.status );
       reject(err);
      });
    });

    return promise;
  }


  /* ----------------- */
  createCinema() {
    if(this.cityExist) {
      this.getCityObjectById(this.selectedCity);
      this.createCinemaService().then((res: any) => {
        this.modalService.dismissAll();
        this.getAllCinemas();
        this.cinema = new Cinema();
        this.newCity = new City();
      });
    } else {
      this.createCityService().then((res: any) => {
        this.cinema.city = res.body.createdCity;
        this.createCinemaService().then((res: any) => {
          this.modalService.dismissAll();
          this.getAllCinemas();
          this.cinema = new Cinema();
          this.newCity = new City();
        });
      });
    }
    
  }

  /* ----------------- */
  updateCinema() {
    if(this.cityExist) {
      this.getCityObjectById(this.selectedCity);
      this.updateCinemaService().then((res: any) => {
        this.modalService.dismissAll();
        this.getAllCinemas();
      });
    } else {
      this.createCityService().then((res: any) => {
        this.cinema.city = res.body.createdCity;
        this.updateCinemaService().then((res: any) => {
          this.modalService.dismissAll();
          this.getAllCinemas();
        });
      });
    }
    
  }

  /* ------------------ */
  getCityObjectById(id: string | String | undefined) {
    this.cinema.city = this.cities.filter(
      c => c._id == id
    )[0];
  }

  /* ----------------------- */
  createCinemaService() {
    let promise = new Promise((resolve, reject) => {
      this.cinemaService.createCinema(this.cinema)
     .subscribe((response: any) => {
       //console.log(response);
       resolve(response);
       } ,
     err => {
       //console.log(  err.status );
       reject(err);
      });
    });

    return promise;
  }

  /* ----------------------- */
  updateCinemaService() {
    let promise = new Promise((resolve, reject) => {
      this.cinemaService.updateCinema(this.cinema)
     .subscribe((response: any) => {
       //console.log(response);
       resolve(response);
       } ,
     err => {
       //console.log(  err.status );
       reject(err);
      });
    });

    return promise;
  }

  /* ----------------------- */
  createCityService() {
    let promise = new Promise((resolve, reject) => {
      this.cityService.createCity(this.newCity)
     .subscribe((response: any) => {
       //console.log(response);
       resolve(response);
       } ,
       (err: any) => {
       //console.log(  err.status );
       reject(err);
      });
    });

    return promise;
  }



  //-----------------------

  onAutocompleteSelected(result: PlaceResult) {
    //console.log('onAutocompleteSelected: ', result);
    this.cinema.address = result.formatted_address;
  }

  onLocationSelected(location: Location) {
    //console.log('onLocationSelected: ', location);
    this.cinema.latitude = location.latitude.toString();
    this.cinema.longitude = location.longitude.toString();
  }

}

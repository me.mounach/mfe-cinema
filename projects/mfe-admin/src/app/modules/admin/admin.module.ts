import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from '../../app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CinemasComponent } from '../../components/cinemas/cinemas.component';
import { AgmCoreModule } from '@agm/core';
import { FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatSelectModule } from '@angular/material/select';
import { StorageServiceModule } from 'ngx-webstorage-service';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { CinemaComponent } from '../../components/cinema/cinema.component';
import { SessionsComponent } from '../../components/sessions/sessions.component';
import { TheatersComponent } from '../../components/theaters/theaters.component';
import { MoviesComponent } from '../../components/movies/movies.component';
import { BrowserAnimationsModule, NoopAnimationsModule } from "@angular/platform-browser/animations";

@NgModule({
  declarations: [
    CinemasComponent,
    CinemaComponent,
    SessionsComponent,
    TheatersComponent,
    MoviesComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule,
    HttpClientModule,
    FlexLayoutModule,
    MatCardModule,
    MatButtonModule,
    MatDividerModule,
    FormsModule,
    //BrowserAnimationsModule,
    MatSelectModule,
    StorageServiceModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDl9Nq14tlbjMSBDk7vKiYB3_eGyxDn_KM',
      libraries: ['places']
    }),
    MatGoogleMapsAutocompleteModule,
    MatTabsModule,
    MatExpansionModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule
  ]
})
export class AdminModule { }

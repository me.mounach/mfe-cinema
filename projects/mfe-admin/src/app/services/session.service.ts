import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Session } from '../models/session/session';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  /* -------- */
  private API_URL = environment.API_URL + "/api/sessions";


  /* ----- */
  constructor(private httpClient: HttpClient) { }


  /* ------------- */
  createSession(session: Session) {

    return this.httpClient.post(this.API_URL ,
    JSON.stringify(session) ,
      {
        headers:  new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json'),
        observe: 'response',
        withCredentials: true
      }
    );
    
  }

  /* ------------- */
  getOneSession(id: string) {
    
    return this.httpClient.get(this.API_URL + "/" + id ,
      {
        headers:  new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json'),
        observe: 'response',
        withCredentials: true
      }
    );
    
  }

  /* ------------- */
  getSessionTickets(id: string) {
    
    return this.httpClient.get(this.API_URL + "/" + id + "/tickets" ,
      {
        headers:  new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json'),
        observe: 'response',
        withCredentials: true
      }
    );
    
  }


  /* ------------- */
  deleteSession(id: string) {
    
    return this.httpClient.delete(this.API_URL + "/" + id,
      {
        headers:  new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json'),
        observe: 'response',
        withCredentials: true
      }
    );
    
  }
}

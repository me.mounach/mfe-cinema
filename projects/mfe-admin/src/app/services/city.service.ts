import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  /* -------- */
  private API_URL = environment.API_URL + "/api/cities";


  /* ----- */
  constructor(private httpClient: HttpClient) { }

  /* ------------- */
  getAllCities() {
    
    return this.httpClient.get(this.API_URL,
      {
        headers:  new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json'),
        observe: 'response',
        withCredentials: true
      }
    );
    
  }

  /* ------------- */
  createCity(city: any) {

    return this.httpClient.post(this.API_URL ,
    JSON.stringify(city) ,
      {
        headers:  new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json'),
        observe: 'response',
        withCredentials: true
      }
    );
    
  }
}

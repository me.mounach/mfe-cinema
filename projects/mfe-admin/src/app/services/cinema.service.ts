import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Cinema } from 'projects/mfe-home/src/app/models/cinema/cinema';

@Injectable({
  providedIn: 'root'
})
export class CinemaService {

  /* -------- */
  private API_URL = environment.API_URL + "/api/cinemas";

  /* ----- */
  constructor(private httpClient: HttpClient) { }

  /* ------------- */
  getAllCinemas() {
    
    return this.httpClient.get(this.API_URL,
      {
        headers:  new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json'),
        observe: 'response',
        withCredentials: true
      }
    );
    
  }

  /* ------------- */
  getCinemaTheaters(id: string) {
    
    return this.httpClient.get(this.API_URL + "/" + id + "/theaters",
      {
        headers:  new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json'),
        observe: 'response',
        withCredentials: true
      }
    );
    
  }

  /* ------------- */
  getCinemaSession(id: string) {
    
    return this.httpClient.get(this.API_URL + "/" + id + "/sessions",
      {
        headers:  new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json'),
        observe: 'response',
        withCredentials: true
      }
    );
    
  }

  /* ------------- */
  getCinemaSessionByDate(id: string, start_date: string, end_date: string) {
    
    return this.httpClient.get(this.API_URL + "/" + id + "/sessions?start_date="+start_date
                                            +"&end_date="+end_date,
      {
        headers:  new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json'),
        observe: 'response',
        withCredentials: true
      }
    );
    
  }

  /* ------------- */
  getOneCinema(id: string) {
    
    return this.httpClient.get(this.API_URL + "/" + id ,
      {
        headers:  new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json'),
        observe: 'response',
        withCredentials: true
      }
    );
    
  }


  /* ------------- */
  deleteCinema(id: string) {
    
    return this.httpClient.delete(this.API_URL + "/" + id,
      {
        headers:  new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json'),
        observe: 'response',
        withCredentials: true
      }
    );
    
  }

  /* ------------- */
  createCinema(cinema: any) {

    return this.httpClient.post(this.API_URL,
    JSON.stringify(cinema) ,
      {
        headers:  new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json'),
        observe: 'response',
        withCredentials: true
      }
    );
    
  }

  /* ------------- */
  updateCinema(cinema: Cinema) {

    return this.httpClient.put(this.API_URL + "/" + cinema._id ,
    JSON.stringify(cinema) ,
      {
        headers:  new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json'),
        observe: 'response',
        withCredentials: true
      }
    );
    
  }

}

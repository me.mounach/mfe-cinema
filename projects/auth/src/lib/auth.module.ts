import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AuthComponent } from './auth.component';
import { AuthService } from './services/auth.service';



@NgModule({
  declarations: [
    AuthComponent
  ],
  imports: [
    HttpClientModule
  ],
  exports: [
    AuthComponent
  ],
  providers: [
    AuthService
  ]
})
export class AuthModule { }

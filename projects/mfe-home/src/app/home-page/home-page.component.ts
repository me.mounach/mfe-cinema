import { Component, ElementRef, NgZone, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as L from 'leaflet';
import { Cinema } from '../models/cinema/cinema';
import { CinemaService } from '../services/cinema-service.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  cinemas: Array<Cinema> = new Array();

  constructor(
    private cinemaService: CinemaService,
    private router: Router,
    private el: ElementRef
  ) { 
  }

  ngOnInit(): void {
    const cinemaMap = L.map('cinemaMap').setView([48.8534, 2.3488], 11);
 
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: 'Map'
    }).addTo(cinemaMap);

    this.getAllCinemas().then((res: any) => {
      this.initMarkers(cinemaMap);
    });
  }

  initMarkers(cinemaMap: L.Map | L.LayerGroup<any>) {

    const myIcon = L.icon({
      iconUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.2.0/images/marker-icon.png'
    });

    this.cinemas.forEach(c => {  

      L.marker([(c.latitude ? +c.latitude : 0), (c.longitude ? +c.longitude : 0)], {icon: myIcon})
       .bindPopup(
         '<div class="cine_'+c._id+'" onmouseover="this.style.opacity=0.5;" onmouseout="this.style.opacity=1;"><a'+
         ' class="cine_'+c._id+'" style="color: rgb(71, 71, 71);cursor: pointer;font-size: 2em;text-decoration: none;">'+
         (c.name ? c.name.toString() : '')
        +'<a></br>'+
        '<a style="cursor: pointer;" class="cine_'+c._id+'">'+
        '<img class="cine_'+c._id+'" style="display: block;margin-left: auto;margin-right: auto;" src="../../../assets/cinema.png" width="150" alt="cinema">'
        +'</a></div>'
       )
       .addTo(cinemaMap)
       .openPopup()
       .on('popupopen' , () => {
        
        $(() => {
          $("div[class*='cine_'")!.click( (event: any) => {
            let cine_id = $(event.target).attr('class')?.split("cine_").pop();
            this.router.navigate(['/cinema/'+cine_id+'/sessions']);
          });
        });

      }); 

    });

    $(() => {
      $("div[class*='cine_'")!.click( (event: any) => {
        let cine_id = $(event.target).attr('class')?.split("cine_").pop();
        this.router.navigate(['/cinema/'+cine_id+'/sessions']);
      });
   });
    
  }


   /* ----------------------- */
   getAllCinemas() {

    let promise = new Promise((resolve, reject) => {
      this.cinemaService.getAllCinemas()
     .subscribe((response: any) => {
       console.log(response);
       this.cinemas = response.body.cinemas;
       resolve(response);
       } ,
     err => {
       //console.log(  err.status );
       reject(err);
      });
    });

    return promise;
  }

}

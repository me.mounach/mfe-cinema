import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('mfeClient/Module').then(m => m.ClientModule)
  },
  {
    path: '',
    loadChildren: () => import('mfeAdmin/Module').then(m => m.AdminModule)
  },
  { path: 'login', component: LoginComponent },
  {
    path: '',
    loadChildren: () => import('mfeHome/Module').then(m => m.HomeModule)
  },
  { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class QrcodeService {

  /* -------- */
  private API_URL = environment.API_URL + "/api/qrcode";


  /* ----- */
  constructor(private httpClient: HttpClient) { }


  /* ------------- */
  createQrcode(ticket: any) {

    return this.httpClient.post(this.API_URL ,
    JSON.stringify(ticket) ,
      {
        headers:  new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json'),
        observe: 'response'
      }
    );
    
  }
}

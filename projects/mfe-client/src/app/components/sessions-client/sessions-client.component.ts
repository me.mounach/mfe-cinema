import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Cinema } from '../../models/cinema/cinema';
import { Session } from '../../models/session/session';
import { CinemaService } from '../../services/cinema.service';

@Component({
  selector: 'app-sessions-client',
  templateUrl: './sessions-client.component.html',
  styleUrls: ['./sessions-client.component.scss']
})
export class SessionsClientComponent implements OnInit {

  /* ----------------------- */
  cinema_id: any;
  cinema: Cinema = new Cinema();
  date_picked!: NgbDateStruct;
  date_today!: NgbDateStruct;
  sessions: Array<Session> = new Array();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private cinemaService: CinemaService,
    private calendar: NgbCalendar
  ) { }

  ngOnInit() {

    this.cinema_id = this.route.snapshot.paramMap.get("id");
    this.getCinemaInfos();

    this.date_today = this.calendar.getToday();
    this.date_picked = this.calendar.getToday();
    this.datePicked();

  }

  /* ----------------------- */
  getCinemaInfos() {

    let promise = new Promise((resolve, reject) => {
      this.cinemaService.getOneCinema(this.cinema_id)
     .subscribe((response: any) => {
       //console.log(response);
       this.cinema = response.body.cinema;
       resolve(response);
       } ,
       (err: any) => {
       //console.log(  err.status );
       reject(err);
      });
    });

    return promise;
  }

  /* ----------------------- */
  getCinemaSessionsByDate(start_date: string, end_date: string) {

    let promise = new Promise((resolve, reject) => {
      this.cinemaService.getCinemaSessionByDate(this.cinema_id, start_date, end_date)
     .subscribe((response: any) => {
       //console.log(response);
       this.sessions = response.body.sessions;
       resolve(response);
       } ,
       (err: any) => {
       //console.log(  err.status );
       reject(err);
      });
    });

    return promise;
  }

  //-----------------

  formatDate_pick(dt: NgbDateStruct){
    
    return `${
      dt.year.toString().padStart(4, '0')}-${
      dt.month.toString().padStart(2, '0')}-${
      dt.day.toString().padStart(2, '0')}`;
    
  }

  datePicked(){
    let start_date = this.formatDate_pick(this.date_picked) + " 00:00:00"; 
    let end_date = this.formatDate_pick(this.date_picked) + " 23:59:00";
    this.getCinemaSessionsByDate(start_date, end_date);
  }

  //---------------
  gotoSession(s_id: String) {
    this.router.navigate(['/session/'+s_id]);
  }

}
